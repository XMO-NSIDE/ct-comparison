# Qlik Application Repository 
# comparison_default
### 
Created By XMO-NSIDE(Xavier Morosini) at Mon Jan 10 2022 17:32:11 GMT+0100 (Central European Standard Time)




Sheet Title | Description
------------ | -------------
KPIs overview (future)|
IMP release plans|Comparison of supply plan for two results.
IRT configuration|Comparison of IRT configuration for two results.
Recruitment|Comparison of enrollment, randomization and completion between two results.
Risk|Comparison of missed dispensing and out of stocks for two results.
Dispensing|Comparison of patient demand for two results.
Site shipments|Comparison of shipments to sites for two results.
Welcome|Welcome page - Specifities - Version - Simulation parameters
Depot shipments|Comparison of forced shipments for two results.



Branch Name|Qlik application
---|---
master|[https://qlik-dev-feb19.n-side.com/sense/app/334ab6fb-1469-4944-9bb1-7d4f4431021a](https://qlik-dev-feb19.n-side.com/sense/app/334ab6fb-1469-4944-9bb1-7d4f4431021a)
leverage-past-data|[https://qlik-dev-feb19.n-side.com/sense/app/9207f488-24a3-4b7e-a945-d436c7633624](https://qlik-dev-feb19.n-side.com/sense/app/9207f488-24a3-4b7e-a945-d436c7633624)
qlik-nov21-upgrade|[https://qlik-dev-nov21.n-side.com/sense/app/f9be5fc0-e08b-4a22-8a03-e2c41e00d834](https://qlik-dev-nov21.n-side.com/sense/app/f9be5fc0-e08b-4a22-8a03-e2c41e00d834)

Branch Name|Qlik application
---|---
release_4_4|[https://qlik-dev-nov21.n-side.com/sense/app/6bcbf7d5-f889-48e0-bc53-4f54ec5fa3dc](https://qlik-dev-nov21.n-side.com/sense/app/6bcbf7d5-f889-48e0-bc53-4f54ec5fa3dc)

Branch Name|Qlik application
---|---
comparison_4_4|[https://qlik-dev-nov21.n-side.com/sense/app/e932c399-0835-458c-b3d8-80b7c7a899b4](https://qlik-dev-nov21.n-side.com/sense/app/e932c399-0835-458c-b3d8-80b7c7a899b4)

Branch Name|Qlik application
---|---
maintenance|[https://qlik-dev-nov21.n-side.com/sense/app/2866f6fb-0210-4e56-b164-f2536e055716](https://qlik-dev-nov21.n-side.com/sense/app/2866f6fb-0210-4e56-b164-f2536e055716)